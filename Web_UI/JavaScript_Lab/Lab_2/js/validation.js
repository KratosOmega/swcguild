function checkValidation(){
    var nameCheck = document.myForm.name.value;
    var emailCheck = document.myForm.email.value;
    var phoneCheck = document.myForm.phone.value;
    var inquiryCheck = document.myForm.inquiry_reason.value;
    var mon = document.myForm.monday.checked;
    var tue = document.myForm.tuesday.checked;
    var wed = document.myForm.wednesday.checked;
    var thu = document.myForm.thursday.checked;
    var fri = document.myForm.friday.checked;

    if (nameCheck== ""){
        alert("[Name] field can not be empty!");
        return false;
    }

    if (emailCheck == "" && phoneCheck == ""){
        alert("Either [e-mail] or [phone] field can not be empty!");
        return false;
    }

    if (inquiryCheck == "Other"){
        var additionalInfor = document.myForm.additional_infor.value;
        if (additionalInfor == ""){
            alert("[Additional Informaiton] field can not be empty!");
            return false;
        }
    }

    if (!mon && !tue && !wed && !thu && !fri){
        alert("[Best days to contact you] field needs at least 1 weekday to be checked!")
        return false;
    }

    alert("Your form has been submitted successfully!");
}
