function roll(){
    var currentBalance = Number(document.getElementById("startingBet").value);
    var startingBet = Number(document.getElementById("startingBet").value);
    var diceA, diceB;
    var totalRolls = 0;
    var highest = 0;
    var rollCountAtHighest = 0;

    //Calculate & Loop
    while (currentBalance >0){
        diceA = Math.floor((Math.random() * 6) + 1);
        diceB = Math.floor((Math.random() * 6) + 1);
        if (diceA + diceB == 7){
            currentBalance += 4.00;
        }else {
            currentBalance -= 1.00;
        }

        totalRolls++;

        if (currentBalance > highest){
            highest = currentBalance;
            rollCountAtHighest = totalRolls;
        }
    }

    //Fill in the result fields
    document.getElementById("startingBet").value = null;
    document.getElementById("resultStartingBet").innerHTML = "$ " + startingBet.toFixed(2);
    document.getElementById("resultTotalRolls").innerHTML = totalRolls;
    document.getElementById("resultHighest").innerHTML = "$ " + highest.toFixed(2);
    document.getElementById("resultRollCount").innerHTML = rollCountAtHighest;
    document.getElementById("roll").innerHTML = "Play Again";

    //Display the result table
    document.getElementById("result").style.display = "block";
}
